#ifndef BARO_CALIBRATION_MANAGER_H
#define BARO_CALIBRATION_MANAGER_H

#include <stdint.h>
#include <fcntl.h> 
#include <sys/stat.h>
#include <sys/wait.h>
#include <fstream>

#include <algorithm>
#include <vector>

#define BaroCalibrationClaimString "/baro"

typedef struct baro_calibration_params {
    uint8_t msg_index;
    float param_vals[11];
} __attribute__((packed)) baro_calibration_params;

typedef struct baro_calibration_progress {
    uint8_t msg_index;
    uint16_t pos;
} __attribute__((packed)) baro_calibration_progress;

static uint8_t autopilot_monitor_get_sysid(void);

void BaroCalibrationCallback(struct mg_connection *c, int ev, void *ev_data, void *fn_data);

void run_baro_calibration();
static void *run_baro_calibration_helper(void *arg);
void process_baro_log();
std::vector<std::string> split_line(std::string input);
void get_baro_params();

void send_baro_calibration_params(struct baro_calibration_params baroCalParamsMsg);
void send_baro_calibration_progress(uint16_t pos);
void save_baro_calibration_params();
void save_baro_calibration_params_helper();
void read_baro_cal_progress(void* args);

#endif